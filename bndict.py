#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
from PySide import QtGui, QtCore

class BanglaDict(QtGui.QWidget):

    def __init__(self):

        super(BanglaDict, self).__init__()
        self.initUI()

    def initUI(self):

        vbox = QtGui.QVBoxLayout(self)
        textbox = QtGui.QLineEdit(self)
        self.image_stream = QtGui.QPixmap()
        self.image_display = QtGui.QLabel(self)

        self.image_display.setPixmap(self.image_stream)

        vbox.addWidget(textbox)
        vbox.addWidget(self.image_display)

        textbox.textChanged[str].connect(self.onTextChange)

        self.setGeometry(300,300,280,170)
        self.setWindowTitle("pyBanglaDictionary")
        self.show()

    def onTextChange(self, text):

        self.image_stream = QtGui.QPixmap(os.path.join('data', text.upper()))
        self.image_display.setPixmap(self.image_stream)


def main():
    app = QtGui.QApplication(sys.argv)
    dictapp = BanglaDict()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
