pyBanglaDictionary
==================

An English-Bangla dictionary written in Python and PySide.

##Running
* First install PySide on your linux distribution. In debian or it's derivatives,

```bash
sudo apt-get install python-pyside
```

* Make the bndict.py file executable.
* Place the image files named after the english words they refer to in a subdirectory called 'data'.
* Double click 'bndict.py' and run.